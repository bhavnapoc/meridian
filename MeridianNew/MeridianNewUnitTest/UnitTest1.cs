using MeridianNew.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MeridianNewUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        ValuesController v = new ValuesController();
        [TestMethod]
        public void TestMethod1()
        {
            //Assert
            var v1 = v.Get(2);
            // Expected
            string v2 = "value2";
            Assert.AreEqual(v1, v2);

        }
        [TestMethod]
        public void Get()
        {
            //Assert
            var v1 = v.Get(2);
            // Expected
            string v2 = "value2";
            Assert.AreEqual(v1, v2);
        }
        [TestMethod]
        public void ReturnId()
        {
            var v2 = v.ReturnId();
            var v3 = "sandeep";
            Assert.AreEqual(v2, v3);
        }

    }
}
