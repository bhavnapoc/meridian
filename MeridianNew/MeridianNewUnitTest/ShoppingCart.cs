﻿using MeridianNew.App_Code;
using MeridianNew.Controllers;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MeridianNewUnitTest
{
   
  public  class ShoppingCartFake : IShoppingCartService
    {
        private readonly List<ShoppingItem> _shoppingCart;
        public ShoppingCartFake()
        {
            _shoppingCart = new List<ShoppingItem>()
            {
                new ShoppingItem() { Id = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200"),
                    Name = "Orange Juice", Manufacturer="Orange Tree", Price = 5.00M },
                new ShoppingItem() { Id = new Guid("815accac-fd5b-478a-a9d6-f171a2f6ae7f"),
                    Name = "Diary Milk", Manufacturer="Cow", Price = 4.00M },
                new ShoppingItem() { Id = new Guid("33704c4a-5b87-464c-bfb6-51971b4d18ad"),
                    Name = "Frozen Pizza", Manufacturer="Uncle Mickey", Price = 12.00M }
            };
        }
        
        public List<ShoppingItem> GetAllItems()
        {
            return _shoppingCart;
        }
       
        public ShoppingItem Add(ShoppingItem newItem)
        {
            newItem.Id = Guid.NewGuid();
            _shoppingCart.Add(newItem);
            return newItem;
        }
        
        public ShoppingItem GetById(Guid id)
        {
            return _shoppingCart.Where(a => a.Id == id)
                .FirstOrDefault();
        }
        
        public void Remove(Guid id)
        {
            var existing = _shoppingCart.First(a => a.Id == id);
            _shoppingCart.Remove(existing);
        }

    }

    [TestFixture]
    public class ShoppingCartControllerTest
    {
        ShoppingController _controller;
        IShoppingCartService _service;

        public ShoppingCartControllerTest()
        {
            _service = new ShoppingCartFake();
            _controller = new ShoppingController(_service);
        }

        [Test]
        public void Get_WhenCalled_Match_With_Count()
        {
            var fake = new ShoppingCartFake();
            // Act
            var okResult = _controller.Get();
            var ok1 = okResult as OkObjectResult;
            var ok2 = (List<ShoppingItem>) ok1.Value;
            // expected
            var expected = fake.GetAllItems();

            // Assert
            Assert.AreEqual(expected.Count, ok2.Count); 
        }
        [Test]
        public void Get_WhenCalled_With_ID_Sould_Match_With_Name()
        {
            var fake = new ShoppingCartFake();
            // Act
            var guid = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200");
            var okResult = _controller.Get(guid);
            var ok1 = okResult as OkObjectResult;
            var ok2 = (ShoppingItem)ok1.Value;
            // exected=
            var shpitem = new ShoppingItem()
            {
                Id = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200"),
                Name = "Orange Juice",
                Manufacturer = "Orange Tree",
                Price = 5.00M
            };
            // Assert
            Assert.AreEqual(ok2.Name, shpitem.Name);

        }

    }
}
